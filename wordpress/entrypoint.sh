#!/bin/bash
set -eo pipefail

if [ ! -f /var/www/html/wp-config.php ]; then
    echo "No wordpress install found! Installing latest wordpress!!!"
    cd /var/www/html
    rm -rf /var/www/html/*
    wget https://wordpress.org/latest.tar.gz
    tar -xzf latest.tar.gz
    mv wordpress/* .
    rm -rf wordpress latest.tar.gz
    chown -R www-data:www-data /var/www/html
    cp wp-config-sample.php wp-config.php
    echo Replacing username_here/$MYSQL_USER
    sed -i "s/username_here/$MYSQL_USER/" wp-config.php
    sed -i "s/password_here/$MYSQL_ROOT_PASSWORD/" wp-config.php
    sed -i "s/localhost/$MY_DOMAIN_NAME/" wp-config.php
    sed -i "s/'utf8'/'utf8mb4'/" wp-config.php

fi

exec "$@"
